package ru.bevz.vendor_machine.component.finance.virtual;

import java.math.BigDecimal;

public interface MoneyDepositor {

    void deposit(BigDecimal value);

}
