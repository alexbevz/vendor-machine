package ru.bevz.vendor_machine.component;

import java.math.BigDecimal;

public interface Product {

    BigDecimal getPrice();

}
